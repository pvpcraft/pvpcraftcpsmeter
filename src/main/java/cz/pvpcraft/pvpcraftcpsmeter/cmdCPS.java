package cz.pvpcraft.pvpcraftcpsmeter;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class cmdCPS implements CommandExecutor {


    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(command.getName().equalsIgnoreCase("cps")) {
            if(!(sender instanceof Player)) {
                sender.sendMessage("You need to be in game to measure CPS");
                return false;
            }

            if(!sender.hasPermission("cps.check") && !sender.isOp()) {
                sender.sendMessage("You do not have permission to check player's CPS");
                return false;
            }

            if(args.length == 0) {
                sender.sendMessage("Usage: /cps {player} [time(Default=10Seconds)]");
            }

            if(args.length == 1 || args.length == 2) {
                int time = 10;
                if(args.length == 2) {
                    time = Integer.parseInt(args[1]);
                }

                Player p = Bukkit.getPlayer(args[0]);

                if(p == null) {
                    sender.sendMessage("Player not found");
                    return false;
                }

                Main.instance.getClickListener().monitorPlayer(sender, p, time);
            }


        }
        return false;
    }
}
