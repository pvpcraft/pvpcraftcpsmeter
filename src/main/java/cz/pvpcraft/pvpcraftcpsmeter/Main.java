package cz.pvpcraft.pvpcraftcpsmeter;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    private clickListener clickListener;

    @Override
    public void onEnable() {
        instance = this;
        clickListener = new clickListener();

        //configLoader.setYamls();

        getServer().getPluginManager().registerEvents(clickListener, this);
        getCommand("cps").setExecutor(new cmdCPS());
    }

    public clickListener getClickListener() {
        return clickListener;
    }
}
