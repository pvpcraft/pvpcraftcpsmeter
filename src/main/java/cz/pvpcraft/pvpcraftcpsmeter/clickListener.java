package cz.pvpcraft.pvpcraftcpsmeter;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class clickListener implements Listener {

    private Map<UUID, Integer> playerClicks;

    public clickListener() {
        playerClicks = new HashMap<UUID, Integer>();
    }

    public void monitorPlayer(CommandSender monitorer, Player victim, int time) {
        if(playerClicks.get(victim.getUniqueId()) != null) {
            return;
        }
        playerClicks.put(victim.getUniqueId(), 0);
        startCheck(time, victim.getUniqueId(), Bukkit.getPlayer(monitorer.getName()));
    }

    private void startCheck(final int time, final UUID victim, final Player monitorer) {

        new BukkitRunnable() {
            int timer = 0;
            public void run()
            {
                if(timer > time) {
                    monitorer.sendMessage("Checking done");
                    playerClicks.remove(victim);
                    cancel();
                }

                if(playerClicks.get(victim) != null && monitorer != null) {
                    monitorer.sendMessage("CPS/1sec: " + playerClicks.get(victim));
                    playerClicks.put(victim, 0);
                }

                timer++;
            }
        }.runTaskTimer(Main.instance, 0, 20);
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(playerClicks.get(p) != null) {
            if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR) {
                playerClicks.put(p.getUniqueId(), playerClicks.get(p)+1);
            }
        }
    }
}
